#!/usr/bin/env python3
import argparse
import os
import sys
from stat import S_ISDIR, S_ISREG


def visit(directory):
    dirs = []

    for entry in os.listdir(directory):
        entry_path = (directory + "/" + entry).replace("//","/")
        stat = os.stat(entry_path)
        mode = stat.st_mode
        if S_ISDIR(mode):
            dirs.append(entry_path)
        elif S_ISREG(mode):
            pass
        else:
            sys.stderr.write("WARNING:" + entry_path + " is neither directory nor a regular file!")

    dirs.sort()
    for i in range(len(dirs) - 1):
        if dirs[i] + "_de" == dirs[i+1]:
            sys.stdout.write(dirs[i] + '\n')
            sys.stdout.write(dirs[i+1] + '\n')

    if args.recursive:
        for d in dirs:
            visit(d)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--recursive', action = 'store_true', help = "dive recursively into folders")
    parser.add_argument("path", nargs='?', default='.')
    args = parser.parse_args()

    visit(args.path)
